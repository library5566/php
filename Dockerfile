FROM alpine:3.8

# Environments
ENV TIMEZONE            Asia/Taipei
ENV PHP_MEMORY_LIMIT    512M
ENV MAX_UPLOAD          50M
ENV PHP_MAX_FILE_UPLOAD 200
ENV PHP_MAX_POST        100M

# Install packages
RUN	apk update && \
	apk upgrade && \
	apk add --update tzdata && \
	cp /usr/share/zoneinfo/${TIMEZONE} /etc/localtime && \
	echo "${TIMEZONE}" > /etc/timezone && \
	apk add --update \
	    screen \
	    curl \
        php5-cli \
		php5-ctype \
		php5-mcrypt \
		php5-openssl \
		php5-gmp \
		php5-json \
		php5-dom \
		php5-pdo \
		php5-zip \
		php5-mysql \
		php5-mysqli \
		php5-sqlite3 \
		php5-bcmath \
		php5-gd \
		php5-pdo_mysql \
		php5-pdo_sqlite \
		php5-gettext \
		php5-xml \
		php5-xmlreader \
		php5-xmlrpc \
		php5-bz2 \
		php5-iconv \
		php5-curl \
		php5-fpm \
        php5-opcache \
        php5-ftp \
        php5-sysvmsg \
        php5-wddx \
        php5-sockets \
		php5-pcntl

# Set environments
RUN \
    sed -i "s|;*daemonize\s*=\s*yes|daemonize = no|g" /etc/php5/php-fpm.conf && \
    sed -i "s|;*listen\s*=\s*127.0.0.1:9000|listen = 9000|g" /etc/php5/php-fpm.conf && \
    sed -i "s|;*listen\s*=\s*/||g" /etc/php5/php-fpm.conf && \
    sed -i "s|;*date.timezone =.*|date.timezone = ${TIMEZONE}|i" /etc/php5/php.ini && \
    sed -i "s|;*memory_limit =.*|memory_limit = ${PHP_MEMORY_LIMIT}|i" /etc/php5/php.ini && \
    sed -i "s|;*upload_max_filesize =.*|upload_max_filesize = ${MAX_UPLOAD}|i" /etc/php5/php.ini && \
    sed -i "s|;*max_file_uploads =.*|max_file_uploads = ${PHP_MAX_FILE_UPLOAD}|i" /etc/php5/php.ini && \
    sed -i "s|;*post_max_size =.*|post_max_size = ${PHP_MAX_POST}|i" /etc/php5/php.ini && \
    sed -i "s|;*cgi.fix_pathinfo=.*|cgi.fix_pathinfo= 0|i" /etc/php5/php.ini

# Cleaning up
RUN \
    mkdir -p /var/www/html && \
    apk del tzdata && \
    rm -rf /var/cache/apk/*

# Set Workdir
WORKDIR /var/www/html
